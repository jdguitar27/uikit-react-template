import React from 'react';
import Header from './components/Header';

class App extends React.Component{
	
	render (){
		return(
			<div>
				<Header data={ {"name" : "José", "lastname":"Durán"} }/>
				<div className="uk-container"><h1>Plantilla react con UIkit</h1></div>
			</div>
		);
	}
}

export default App;
